require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'syntaxi'

DataMapper::Database.setup({
  :adapter  => 'mysql',
  :host     => 'localhost',
  :username => 'root',
  :password => '',
  :database => 'toopaste_development'
})

class Snippet < DataMapper::Base
  property :body, :text
  property :created_at, :datetime
  property :updated_at, :datetime
  
  validates_presence_of :body
  validates_length_of :body, :minimum => 1
  
  Syntaxi.line_number_method = 'floating'
  
  def formatted_body
    html = Syntaxi.new("[code lang='ruby']#{self.body}[/code]").process
    "<div class=\"syntax syntax_ruby\">#{html}</div>"
  end
end

database.table_exists?(Snippet) or database.save(Snippet)

layout 'default.erb'

# new
get '/' do
  erb :new, :layout => 'default.erb'
end

# create
post '/' do
  @snippet = Snippet.new(:body => params[:snippet_body])
  if @snippet.save
    redirect "/#{@snippet.id}"
  else
    redirect '/'
  end
end

# show
get '/:id' do
  @snippet = Snippet.find(params[:id])
  erb :show, :layout => 'default.erb'
end
